import sbt._
import play.Play.autoImport._

object Version {
  val akka      = "2.3.4"
  val scalaTest = "2.2.4"
  val xivojavactilib = "0.0.33.0"
  val mockito = "1.10.19"
  val xucstats = "2.3.3"
  val xucami = "1.11.1"
  val playauthentication = "2.2"
  val metrics = "3.1.0"
  val dbunit = "2.4.7"
  val scalatestplay = "1.2.0"
  val selenium = "2.31.0"
  val htmlcompressor = "1.4"
  val yuicompressor = "2.4.6"
  val closurecompiler = "r1043"
}

object Library {
  val xivojavactilib     = "org.xivo"               % "xivo-javactilib"     % Version.xivojavactilib
  val playauthentication = "xivo"                  %% "play-authentication" % Version.playauthentication
  val xucstats           = "xivo"                  %% "xucstats"            % Version.xucstats changing()
  val xucami             = "xivo"                  %% "xucami"              % Version.xucami changing()
  val metrics            = "io.dropwizard.metrics"  % "metrics-core"        % Version.metrics
  val akkaTestkit        = "com.typesafe.akka"     %% "akka-testkit"        % Version.akka
  val scalaTest          = "org.scalatest"         %% "scalatest"           % Version.scalaTest
  val mockito            = "org.mockito"            % "mockito-all"         % Version.mockito
  val dbunit             = "org.dbunit"             % "dbunit"              % Version.dbunit
  val scalatestplay      = "org.scalatestplus"     %% "play"                % Version.scalatestplay
  val selenium        =  "org.seleniumhq.selenium"        %   "selenium-java"       % Version.selenium
  val htmlcompressor  =  "com.googlecode.htmlcompressor"  %   "htmlcompressor"      % Version.htmlcompressor
  val yuicompressor   =  "com.yahoo.platform.yui"         %   "yuicompressor"       % Version.yuicompressor
}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.6"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us"),
    ("Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/")

  )

  val runDep = run(
    xivojavactilib,
    xucstats,
    xucami,
    playauthentication,
    javaWs,
    metrics,
    htmlcompressor,
    yuicompressor
  )

  val testDep = test(
    scalaTest,
    akkaTestkit,
    mockito,
    dbunit,
    scalatestplay,
    selenium
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
