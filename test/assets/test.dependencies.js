EnvJasmine.loadGlobal(EnvJasmine.testDir + "jasminecustommatchers.js");
EnvJasmine.loadGlobal(EnvJasmine.testDir + "mockobjectsbuilder.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "jquery-2.0.3.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "shotgun.js");
var SHOTGUN={};
require(['shotgun'],function(sh){
    SHOTGUN=sh;
    });
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "cti.js");
EnvJasmine.loadGlobal(EnvJasmine.rootDir + "directorydisplay.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "ng-table/ng-table.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "ui-bootstrap-tpls-0.11.0.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-mocks.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-cookies.js");
EnvJasmine.loadGlobal(EnvJasmine.libDir + "angular-local-storage/angular-local-storage.js");
