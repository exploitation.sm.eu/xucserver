package controllers.xuc

import play.api.libs.ws.WSClient
import play.api.libs.ws._
import play.api.test.{FakeApplication, PlaySpecification, FakeRequest, TestServer}
import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._

class WebSocketAppSpec extends PlaySpecification {

  def await[T](future: Future[T]) = Await.result(future, 30 seconds)

  def wsUrl(url: String)(implicit port: Int, client: WSClient = WS.client(play.api.Play.current)) = {
    WS.clientUrl("http://localhost:" + port + url)
  }

  def xivoIntegrationConfig: Map[String, String] = {
    Map(
      "XivoWs.wsUser" -> "xivows",
      "XivoWs.wsPwd" -> "xivows",
      "XivoWs.host" -> "xivo-integration",
      "XivoWs.port" -> "50051",
      "ws.acceptAnyCertificate" -> "true"
    )
  }

  def withServer[A](block: => A): A = {
    val app = FakeApplication( additionalConfiguration = xivoIntegrationConfig )
    running(TestServer(testServerPort, app))(block)
  }

  "WebSocketApp" should {
    "reject request for empty username" in {
      withServer {
        implicit val port = testServerPort
        await(wsUrl("/xuc/ctichannel?username=&agentNumber=0&password=asd").withHeaders(
          "Upgrade" -> "websocket",
          "Connection" -> "Upgrade",
          "Sec-WebSocket-Version" -> "13",
          "Sec-WebSocket-Key" -> "uzmck/1+1A5FQvwCEuwdSA==",
          "Origin" -> "http://xuc.org"
        ).get()).status must_== FORBIDDEN
      }
    }
  }

  "WebSocketApp" should {
    "reject request for empty token" in {
      withServer {
        implicit val port = testServerPort
        await(wsUrl("/xuc/ctichannel/tokenAuthentication?token=").withHeaders(
          "Upgrade" -> "websocket",
          "Connection" -> "Upgrade",
          "Sec-WebSocket-Version" -> "13",
          "Sec-WebSocket-Key" -> "uzmck/1+1A5FQvwCEuwdSA==",
          "Origin" -> "http://xuc.org"
        ).get()).status must_== FORBIDDEN
      }
    }
  }

  "WebSocketApp" should {
    "reject authentication request for unknown token" in {
      withServer {
        implicit val port = testServerPort
        await(wsUrl("/xuc/ctichannel/tokenAuthentication?token=unexisting_authtoken123456").withHeaders(
          "Upgrade" -> "websocket",
          "Connection" -> "Upgrade",
          "Sec-WebSocket-Version" -> "13",
          "Sec-WebSocket-Key" -> "uzmck/1+1A5FQvwCEuwdSA==",
          "Origin" -> "http://xuc.org"
        ).get()).status must_== FORBIDDEN
      }
    }
  }
}

