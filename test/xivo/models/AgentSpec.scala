package xivo.models

import play.api.libs.json._
import play.api.test.{FakeApplication, PlaySpecification}
import xivo.data.dbunit.DBUtil


class AgentSpec extends PlaySpecification {

  def xivoIntegrationConfig: Map[String, String] = {
    Map(
      "XivoWs.wsUser" -> "xivows",
      "XivoWs.wsPwd" -> "xivows",
      "XivoWs.host" -> "192.168.56.3",
      "XivoWs.port" -> "50051",
      "ws.acceptAnyCertificate" -> "true",
      "db.default.driver" -> "org.postgresql.Driver",
      "db.default.url" -> "jdbc:postgresql://127.0.0.1/testdata",
      "db.default.user" -> "test",
      "db.default.password" -> "test"
    )
  }

  "agent" should {
    "be able to be transformed to json" in {
      val a = Agent(34, "john", "doe", "5000", "default")
      val json = Json.toJson(a)
      json should not be (null)
    }

    "be able to be retreived from base by id" in {
      running(FakeApplication(additionalConfiguration = xivoIntegrationConfig)) {
        DBUtil.setupDB("agentfeatures.xml")

        val aline = Agent(2, "Aline", "Belle", "1702", "sales", groupId = 7)
        val ag = Agent.getById(2)

        ag must be_===(Some(aline))
      }
    }
    "return none if agent does not exists" in {
      running(FakeApplication(additionalConfiguration = xivoIntegrationConfig)) {
        DBUtil.setupDB("agentfeatures.xml")

        val ag = Agent.getById(289)

        ag must be_===(None)

      }
    }
    "move an agent to another group" in {
      running(FakeApplication(additionalConfiguration = xivoIntegrationConfig)) {
        DBUtil.setupDB("agentfeatures.xml")

        Agent.moveAgentToGroup(1, 7)

        Agent.getById(1) shouldEqual Some(Agent(1, "Jack", "Wright", "1502", "market", 7))
      }
    }
  }
}
