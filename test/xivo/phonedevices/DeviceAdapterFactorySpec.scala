package xivo.phonedevices

import play.api.test.PlaySpecification
import xivo.models.Line
import xivo.models.XivoDevice

class DeviceAdapterFactorySpec extends PlaySpecification {

  var deviceAdapterFactory: DeviceAdapterFactory = new DeviceAdapterFactory()

  "factory" should {

    "get a default cti adapter when phone type is not known" in {
      val line = Line(56, "default", "sip", "ojhf", Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c",
        Some("10.50.2.104"), "Unknown")))

      val adapter = deviceAdapterFactory.getAdapter(line)

      adapter should haveClass[CtiDevice]

    }

    "get a default cti adapter when no device is present" in {
      val line = Line(56, "default","sip", "ojhf", None)

      val adapter = deviceAdapterFactory.getAdapter(line)

      adapter should haveClass[CtiDevice]

    }
    "get a default cti adapter when no ip address is present" in {
      val line = Line(56, "default", "sip", "ojhf", Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c", None, "Snom")))

      val adapter = deviceAdapterFactory.getAdapter(line)

      adapter should haveClass[CtiDevice]

    }

    "get a Snom adapter when phone type is a Snom" in {
      val line = Line(56, "default", "sip", "ojhf", Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c",
        Some("10.50.2.104"), "Snom")))

      val adapter = deviceAdapterFactory.getAdapter(line)

      adapter should haveClass[SnomDevice]

    }

  }

}