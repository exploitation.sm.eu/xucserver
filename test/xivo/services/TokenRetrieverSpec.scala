package xivo.services

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.{Token, XivoUser}
import org.joda.time.DateTime
import org.scalatest.mock.MockitoSugar
import services.ActorFactory
import services.config.ConfigRepository

import org.mockito.Mockito.stub
import xivo.services.TokenRetriever.TokenByLogin
import xivo.services.XivoAuthentication.{AuthUnknownUser, AuthTimeSynchronizationError, AuthTimeout}
import xivo.xuc.api.{RequestUnauthorized, RequestError, RequestSuccess}

class TokenRetrieverSpec extends TestKitSpec("TokenRetrieverSpec") with MockitoSugar {

  class Helper {
    val xivoAuthentication = TestProbe()
    ConfigRepository.repo = mock[ConfigRepository]

    trait TestActorFactory extends ActorFactory {
      override val xivoAuthenticationURI = xivoAuthentication.ref.path.toString
    }

    def actor() = {
      val a = TestActorRef(new TokenRetriever with TestActorFactory)
      (a, a.underlyingActor)
    }
  }

  "A TokenRetriever" should {
    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetToken request
      | answer with RequestSuccess on success""" in new Helper {
      val login = "j.doe"
      val user = XivoUser(12, "John", Some("Doe"), Some(login), None)
      val token = Token("thetoken", new DateTime(), new DateTime(), "authId", "userId")
      stub(ConfigRepository.repo.getUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetToken(user.id))
      xivoAuthentication.reply(token)
      expectMsg(RequestSuccess(token.token))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetToken request
      | answer with RequestError on AuthTimeout""" in new Helper {
      val login = "j.doe"
      val user = XivoUser(12, "John", Some("Doe"), Some(login), None)
      stub(ConfigRepository.repo.getUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetToken(user.id))
      xivoAuthentication.reply(AuthTimeout)
      expectMsg(RequestError("Token retrieval timeout"))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetToken request
      | answer with RequestError on AuthTimeSynchronizationError""" in new Helper {
      val login = "j.doe"
      val user = XivoUser(12, "John", Some("Doe"), Some(login), None)
      stub(ConfigRepository.repo.getUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetToken(user.id))
      xivoAuthentication.reply(AuthTimeSynchronizationError)
      expectMsg(RequestError("Token time synchronization error"))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetToken request
      | answer with RequestUnauthorized on AuthUnknownUser""" in new Helper {
      val login = "j.doe"
      val user = XivoUser(12, "John", Some("Doe"), Some(login), None)
      stub(ConfigRepository.repo.getUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetToken(user.id))
      xivoAuthentication.reply(AuthUnknownUser)
      expectMsg(RequestUnauthorized(s"User $login unknown by the token server"))
    }

    "answer with RequestUnauthorized if the user is not found in ConfigRepository" in new Helper {
      val login = "j.doe"
      stub(ConfigRepository.repo.getUser(login)).toReturn(None)
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      expectMsg(RequestUnauthorized(s"User $login unknown by the Xuc"))
    }
  }
}
