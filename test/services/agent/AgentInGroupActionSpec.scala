package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito.verify
import services.ActorFactory
import services.agent.AgentInGroupAction.{RemoveAgents, AgentsDestination}
import services.config.ConfigDispatcher._
import xivo.models.Agent

import scala.concurrent.duration.DurationInt

class AgentInGroupActionSpec extends TestKitSpec("agentInGroupAction") with MockitoSugar {

    class Helper {

      val fakeAction = mock[(Agent, Option[Long], Option[Int]) => Unit]

      class AgentInGroupDoIt(groupId: Long, fromQueueId : Long, fromPenalty: Int) extends  AgentInGroupAction(groupId, fromQueueId, fromPenalty) {

        override def actionOnAgent(agent: Agent, toQueueId: Option[Long], toPenalty: Option[Int]) = fakeAction(agent, toQueueId, toPenalty)

      }

      val configDispatcher = TestProbe()

      trait TestActorFactory extends  ActorFactory {
        override val configDispatcherURI = configDispatcher.ref.path.toString
      }

      def actor(groupId: Long, queueId: Long, penalty : Int) = {
        val a = TestActorRef(Props(new AgentInGroupDoIt(groupId, queueId, penalty) with TestActorFactory))
        (a,a.underlyingActor)
      }
    }

    "an agent group action" should {
      "request agents from config dispatcher" in new Helper {
        val (groupId, fromQueueId, fromPenalty) = (1,52,2)
        val (toQueueId, toPenalty) = (85,9)
        val (ref,_) = actor(groupId,fromQueueId,fromPenalty)

        ref ! AgentsDestination(toQueueId,toPenalty)


        configDispatcher.expectMsg(RequestConfig(ref,GetAgents(groupId, fromQueueId, fromPenalty)))
      }

      "should execute action on agents on destination received" in new Helper {

        val (groupId, fromQueueId, fromPenalty) = (7,44,8)
        val (toQueueId, toPenalty) = (22,2)
        val (ref,_) = actor(groupId,fromQueueId,fromPenalty)

        val john = Agent(1,"John","Malt","33784","default",groupId)
        val agents = List(john)

        ref ! AgentsDestination(toQueueId, toPenalty)

        ref !  AgentList(agents)

        verify(fakeAction)(john, Some(toQueueId), Some(toPenalty))

      }
      "should execute action on agents on remove agents" in new Helper {
        val (groupId, queueId, penalty) = (7,44,8)
        val (ref,_) = actor(groupId,queueId,penalty)

        val john = Agent(1,"John","Malt","33784","default",groupId)
        val agents = List(john)

        ref ! RemoveAgents

        ref !  AgentList(agents)

        verify(fakeAction)(john, None, None)

      }

      "should terminate on completion" in new Helper {
        val watcher = TestProbe()

        val (groupId, fromQueueId, fromPenalty) = (7,44,8)
        val (toQueueId, toPenalty) = (fromQueueId,2)
        val (ref,_) = actor(groupId,fromQueueId,fromPenalty)

        val agents = List(Agent(1,"John","Malt","33784","default",groupId))

        watcher.watch(ref)

        ref ! AgentsDestination(toQueueId, toPenalty)

        ref !  AgentList(agents)

        watcher.expectTerminated(ref,500 milliseconds)

      }


    }


  }
