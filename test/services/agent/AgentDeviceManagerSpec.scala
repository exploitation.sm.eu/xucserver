package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.message.request.AgentLogout
import services.request.{TurnOffKeyLight, PhoneKey, TurnOnKeyLight}
import services.{ActorFactory, XucEventBus}
import xivo.events.AgentState.{AgentLoggedOut, AgentReady, AgentOnPause}

class AgentDeviceManagerSpec extends TestKitSpec("AgentDeviceManagerSpec") with MockitoSugar {

  val eventBus = mock[XucEventBus]
  val amiBusConnector = TestProbe()

  class Helper {
      trait TestActorFactory extends ActorFactory {
        override val amiBusConnectorURI = amiBusConnector.ref.path.toString
      }

      def actor = {
        val a = TestActorRef[AgentDeviceManager](Props(new AgentDeviceManager(eventBus) with TestActorFactory))
        (a, a.underlyingActor)
      }
  }

  "An agent device manager " should {
    "subscribe to event bus at startup" in new Helper {

      val (ref, _) = actor

      verify(eventBus).subscribe(ref,XucEventBus.allAgentsEventsTopic)
    }

    "turn on pause light on event agent on pause " in new Helper {
      val phoneNb ="33000"
      val agentOnPause = AgentOnPause(34,null,phoneNb, List())

      val (ref, _) = actor

      ref ! agentOnPause

      amiBusConnector.expectMsg(TurnOnKeyLight(phoneNb,PhoneKey.Pause))

    }
    "Turn off pause light turn on logon light, on event agent ready" in new Helper {
      val phoneNb = "45000"
      val agentReady = AgentReady(89, null, phoneNb, List())

      val (ref, _) = actor

      ref ! agentReady

      amiBusConnector.expectMsgAllOf(TurnOffKeyLight(phoneNb,PhoneKey.Pause), TurnOnKeyLight(phoneNb,PhoneKey.Logon))

    }

    "Turn off pause light and logon light on event agent logged out" in new Helper {
      val phoneNb = "44201"

      val agentLoggedout = AgentLoggedOut(97, null, phoneNb, List())

      val (ref, _) = actor

      ref ! agentLoggedout

      amiBusConnector.expectMsgAllOf(TurnOffKeyLight(phoneNb,PhoneKey.Pause), TurnOffKeyLight(phoneNb,PhoneKey.Logon))
    }

  }


}
