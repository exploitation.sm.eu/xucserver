package services.directory

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models._
import org.scalatest.mock.MockitoSugar
import DirectoryTransformer.RawDirectoryResult
import xivo.services.XivoDirectory.{Action, FavoriteUpdated, DirLookupResult}
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent
import org.mockito.Mockito.when

class DirectoryTransformerSpec extends TestKitSpec("DirectoryTransformerSpec") with MockitoSugar {

  class Helper {
    val logic = mock[DirectoryTransformerLogic]
    def actor() = {
      val a = TestActorRef(new DirectoryTransformer(logic))
      (a, a.underlyingActor)
    }
  }

  "DirectoryTransformer" should {
    """transform xivoDirectory search result to RichDirectoryResult with presence information
      |and send it to the asking actor""".stripMargin in new Helper() {
      val (ref, _) = actor()
      val requester = TestProbe()
      val searchResult = mock[DirSearchResult]
      val richResult = new RichDirectoryResult(List("testHeader"))
      when(logic.addStatusesDirResult(searchResult)).thenReturn(richResult)

      ref ! RawDirectoryResult(requester.ref, DirLookupResult(searchResult))

      requester.expectMsg(WsContent(WebSocketEvent.createEvent(richResult)))
    }
  }
  "transform xivoDirFavorite result to Websocket event and send it to the asking actor" in new Helper() {
    val (ref, _) = actor()
    val requester = TestProbe()
    val searchResult = FavoriteUpdated(Action.Added, "contactId", "sourceDirectory")

    ref ! RawDirectoryResult(requester.ref, searchResult)

    requester.expectMsg(WsContent(WebSocketEvent.createEvent(searchResult)))
  }

}
