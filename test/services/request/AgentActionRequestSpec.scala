package services.request

import play.api.libs.json.{JsError, Json}
import xuctest.BaseTest

class AgentActionRequestSpec extends BaseTest {

  "An agent Login request" should {
    "be read from json" in {

      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin",
        "agentid" -> 78,
        "agentphonenumber" -> "5300"
      )

      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result.get should be(AgentLoginRequest(Some(78), Some("5300")))
    }

    "be read from json with phoneNumber and agentNumber, without AgentId" in {
      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin",
        "agentphonenumber" -> "5540",
        "agentnumber" -> "454"
      )
      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result.get should be(AgentLoginRequest(None, Some("5540"), Some("454")))

    }

    "be read from json with agent id no phoneNumber" in {
      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin",
        "agentid" -> 321
      )

      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result.get should be(AgentLoginRequest(Some(321),None))

    }
    "be read from json no agent id but phoneNumber" in {
      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin",
        "agentphonenumber" -> "4568"
      )

      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result.get should be(AgentLoginRequest(None,Some("4568")))

    }
    "agent phone number can be an int" in {
      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin",
        "agentphonenumber" -> 5250
      )
      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result.get should be(AgentLoginRequest(None,Some("5250")))

    }
    "report an error if no agent id and no phone number" in {
      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin")

      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result shouldBe a[JsError]

    }
    "report an error if agent id is a string and no phone number" in {
      val loginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin",
      "agentid" -> "444"
      )

      val result = AgentLoginRequest.AgentLoginRequestRead.reads(loginRequest)

      result shouldBe a[JsError]

    }
  }

}
