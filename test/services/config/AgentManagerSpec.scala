package services.config

import akkatest.TestKitSpec
import akka.testkit.TestActorRef
import akka.actor.Props
import akka.testkit.TestProbe
import org.mockito.Mockito.stub
import org.mockito.Mockito.verify
import org.mockito.Mockito.times
import org.scalatest.mock.MockitoSugar
import services.AgentActorFactory
import xivo.ami.AgentCallUpdate
import xivo.events.AgentState.{AgentLogin, AgentReady, AgentOnPause}
import org.joda.time.DateTime
import xivo.models.Agent
import xivo.xucami.models.MonitorState

class AgentManagerSpec extends TestKitSpec("AgentManagerSpec")
  with MockitoSugar {

  class Helper {
    val agFSMFactory = mock[AgentActorFactory]
    def actor = {
      val a = TestActorRef[AgentManager](Props(new AgentManager(agFSMFactory)))
      (a, a.underlyingActor)
    }
    def actorWithFsm(agentId: Agent.Id) = {
      val (ref, agentManager) = actor
      val agentFSM = TestProbe()
      agentManager.agFsms += (agentId -> agentFSM.ref)
      (ref, agentManager, agentFSM)
    }
  }

  "Agent Manager" should {
    "create agent FSM on state received" in new Helper {
      val (ref, agentManager) = actor
      val agentState = AgentReady(23, new DateTime(), "1001", List())
      val agentFSM = TestProbe()
      stub(agFSMFactory.getOrCreate(23, agentManager.context)).toReturn(agentFSM.ref)
      ref ! agentState

      verify(agFSMFactory).getOrCreate(23, agentManager.context)
      agentFSM.expectMsg(agentState)
    }
    "send agent user status to the FSM" in new Helper {
      val (ref, agentManager) = actor
      val agentFSM = TestProbe()
      val agentId: Agent.Id = 254
      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))

      val agust = AgentUserStatus(agentId, "en_bateau")

      ref ! agust

      agentFSM.expectMsg(agust)
    }
    "send agent call update to the FSM" in new Helper {
      val agentId: Agent.Id = 665
      val (ref, agentManager) = actor
      val agentFSM = TestProbe()
      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))

      val acu = AgentCallUpdate(agentId, MonitorState.ACTIVE)

      ref ! acu

      agentFSM.expectMsg(acu)
    }
    "get or create agent FSM and agent stat on login received and send event" in new Helper {
      val (ref, agentManager) = actor
      val agl = AgentLogin(54, new DateTime(), "1000", List())
      val agentFSM = TestProbe()
      val agentStatCollector = TestProbe()

      stub(agFSMFactory.getOrCreate(54, agentManager.context)).toReturn(agentFSM.ref)
      stub(agFSMFactory.getOrCreateAgentStatCollector(54, agentManager.context)).toReturn(agentStatCollector.ref)

      ref ! agl

      verify(agFSMFactory).getOrCreate(54, agentManager.context)
      agentFSM.expectMsg(agl)
      agentStatCollector.expectMsg(agl)
    }
  }
}