package services.config

import java.util.UUID

import akkatest.TestKitSpec
import models.{PreferredCallbackPeriod, CallbackList, CallbackRequest}
import org.joda.time.{LocalDate, LocalTime}
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.{stub, verify}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import play.api.libs.json.Json
import play.api.libs.ws.{WSRequestHolder, WSResponse}
import play.api.test.FakeApplication
import play.api.test.Helpers._
import xivo.models.Agent
import xivo.network.{WebServiceException, XiVOWSdef}

import scala.concurrent.Future

class ConfigServerRequesterSpec extends TestKitSpec("ConfigServerRequesterSpec")  with MockitoSugar with ScalaFutures {

  val appliConfig: Map[String, Any] = {
    Map(
      "config.host" -> "configIP",
      "config.port" -> 9000,
      "config.token" -> "abcdef12456"
    )
  }

  implicit val defaultPatience = PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))
  val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  class Helper {
    val fakeApp = FakeApplication(additionalConfiguration = appliConfig)
    val xivoWS: XiVOWSdef = mock[XiVOWSdef]
    val requester = new ConfigServerRequester(xivoWS)
  }

  "A ConfigServerRequester" should {
    "list CallbackLists" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        stub(xivoWS.get("configIP", "api/1.0/callback_lists", port=Some(9000), headers=Map("X-Auth-Token" -> "abcdef12456"), protocol="http")).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        val listUuid = UUID.randomUUID()
        val cbUuid = UUID.randomUUID()
        val periodUuid = UUID.randomUUID()
        stub(response.json).toReturn(Json.parse(s"""[{
                                                   |"uuid":"$listUuid",
                                                   |"name":"The List",
                                                   |"queueId":3,
                                                   |"callbacks":[{
                                                   | "uuid": "$cbUuid",
                                                   | "listUuid":"$listUuid",
                                                   | "phoneNumber": "1000",
                                                   | "company": "The company",
                                                   | "preferredPeriod": {
                                                   |   "uuid": "$periodUuid",
                                                   |   "name": "morning",
                                                   |   "periodStart": "08:00:00",
                                                   |   "periodEnd": "11:00:00",
                                                   |   "default": true
                                                   | },
                                                   | "dueDate": "2015-01-10"
                                                   |}]
                                                   |}]""".stripMargin))

        requester.getCallbackLists.futureValue shouldEqual List(CallbackList(
            Some(listUuid), "The List", 3, List(
              CallbackRequest(
                Some(cbUuid),
                listUuid,
                Some("1000"),
                None,
                None,
                None,
                Some("The company"),
                None,
                preferredPeriod = Some(PreferredCallbackPeriod(
                  Some(periodUuid),
                  "morning",
                  new LocalTime(8, 0, 0),
                  new LocalTime(11, 0, 0),
                  true
                )),
                dueDate = new LocalDate(2015, 1, 10)
              ))
          ))
      }
    }

    "return an error with unparsable JSON" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        stub(xivoWS.get("configIP", "api/1.0/callback_lists", port=Some(9000), headers=Map("X-Auth-Token" -> "abcdef12456"), protocol="http")).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.json).toReturn(Json.parse("{}"))

        var nbTries = 0
        val fut = requester.getCallbackLists
        while(nbTries < 5 && !fut.isCompleted) {
          Thread.sleep(100)
          nbTries += 1
        }
        if(nbTries == 5)
          fail("Future expired")
        else
          fut.eitherValue shouldEqual Some(Left(new WebServiceException("Non understandable JSON returned")))
      }
    }

    "post a csv to the config server and return Unit" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val listUuid = UUID.randomUUID().toString
        val csv = ""
        stub(xivoWS.genericPost("configIP", s"api/1.0/callback_lists/$listUuid/callback_requests/csv", headers=Map("X-Auth-Token" -> "abcdef12456"),
          protocol="http", payload = Some(csv), port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(CREATED)

        whenReady(requester.importCsvCallback(listUuid, csv)) { res => {
            res shouldEqual((): Unit)
            verify(xivoWS).genericPost("configIP", s"api/1.0/callback_lists/$listUuid/callback_requests/csv", Some(csv),
              null, null, Map("X-Auth-Token" -> "abcdef12456"), Some(9000), "http")
          }
        }
      }
    }

    "post a csv to the config server and fail when an error is returned" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val listUuid = UUID.randomUUID().toString
        val csv = ""
        stub(xivoWS.genericPost("configIP", s"api/1.0/callback_lists/$listUuid/callback_requests/csv", headers=Map("X-Auth-Token" -> "abcdef12456"),
          protocol="http", payload = Some(csv), port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(INTERNAL_SERVER_ERROR)
        stub(response.body).toReturn("Error message")

        var nbTries = 0
        val fut = requester.importCsvCallback(listUuid, csv)
        while(nbTries < 5 && !fut.isCompleted) {
          Thread.sleep(100)
          nbTries += 1
        }
        if(nbTries == 5)
          fail("Future expired")
        else
          fut.eitherValue shouldEqual Some(Left(new WebServiceException("Web service call failed with code 500 and message \"Error message\"")))
      }
    }

    "send a take callback request" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID().toString
        val agent = Agent(12, "John", "Doe", "1000", "default")
        stub(xivoWS.post("configIP", s"api/1.0/callback_requests/$uuid/take", payload=Some(Json.obj("agentId" -> agent.id)),
          headers=Map("X-Auth-Token" -> "abcdef12456"), protocol="http", port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(OK)

        whenReady(requester.takeCallback(uuid, agent)) { res =>
          res shouldEqual((): Unit)
          verify(xivoWS).post("configIP", s"api/1.0/callback_requests/$uuid/take", Some(Json.obj("agentId" -> agent.id)),
            null, null, Map("X-Auth-Token" -> "abcdef12456"), Some(9000), "http")
        }
      }
    }

    "send a take callback request and fail when an error is returned" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID().toString
        val agent = Agent(12, "John", "Doe", "1000", "default")
        stub(xivoWS.post("configIP", s"api/1.0/callback_requests/$uuid/take", payload=Some(Json.obj("agentId" -> agent.id)),
          headers=Map("X-Auth-Token" -> "abcdef12456"), protocol="http", port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(BAD_REQUEST)
        stub(response.body).toReturn("Error message")

        var nbTries = 0
        val fut = requester.takeCallback(uuid, agent)
        while(nbTries < 5 && !fut.isCompleted) {
          Thread.sleep(100)
          nbTries += 1
        }
        if(nbTries == 5)
          fail("Future expired")
        else
          fut.eitherValue shouldEqual Some(Left(new WebServiceException("Web service call failed with code 400 and message \"Error message\"")))
      }
    }

    "send a release callback request" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID().toString
        stub(xivoWS.post("configIP", s"api/1.0/callback_requests/$uuid/release", headers=Map("X-Auth-Token" -> "abcdef12456"),
          protocol="http", port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(OK)

        whenReady(requester.releaseCallback(uuid)) { res =>
          res shouldEqual((): Unit)
          verify(xivoWS).post("configIP", s"api/1.0/callback_requests/$uuid/release", null, null, null, Map("X-Auth-Token" -> "abcdef12456"), Some(9000), "http")
        }
      }
    }

    "send a take release request and fail when an error is returned" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID().toString
        stub(xivoWS.post("configIP", s"api/1.0/callback_requests/$uuid/release", headers=Map("X-Auth-Token" -> "abcdef12456"),
          protocol="http", port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(BAD_REQUEST)
        stub(response.body).toReturn("Error message")

        var nbTries = 0
        val fut = requester.releaseCallback(uuid)
        while(nbTries < 5 && !fut.isCompleted) {
          Thread.sleep(100)
          nbTries += 1
        }
        if(nbTries == 5)
          fail("Future expired")
        else
          fut.eitherValue shouldEqual Some(Left(new WebServiceException("Web service call failed with code 400 and message \"Error message\"")))
      }
    }

    "retrieve a CallbackRequest by uuid" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID()
        val listUuid = UUID.randomUUID()
        stub(xivoWS.get("configIP", s"api/1.0/callback_requests/$uuid", port=Some(9000), headers=Map("X-Auth-Token" -> "abcdef12456"), protocol="http"))
          .toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(OK)
        stub(response.json).toReturn(Json.parse(s"""{
                                                   | "uuid": "$uuid",
                                                   | "listUuid":"$listUuid",
                                                   | "phoneNumber": "1000",
                                                   | "company": "The company",
                                                   | "queueId": 23
                                                   |}""".stripMargin))

        requester.getCallbackRequest(uuid.toString).futureValue shouldEqual CallbackRequest(
              Some(uuid),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None,
              queueId=Some(23))
      }
    }

    "retrieve a CallbackRequest and fail if an error code is returned" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID()
        stub(xivoWS.get("configIP", s"api/1.0/callback_requests/$uuid", headers=Map("X-Auth-Token" -> "abcdef12456"),
          protocol="http", port=Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(NOT_FOUND)
        stub(response.body).toReturn("Error message")

        var nbTries = 0
        val fut = requester.getCallbackRequest(uuid.toString)
        while(nbTries < 5 && !fut.isCompleted) {
          Thread.sleep(100)
          nbTries += 1
        }
        if(nbTries == 5)
          fail("Future expired")
        else
          fut.eitherValue shouldEqual Some(Left(new WebServiceException("Web service call failed with code 404 and message \"Error message\"")))
      }
    }

    "cloture a CallbackRequest" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID()
        stub(xivoWS.post("configIP", s"api/1.0/callback_requests/$uuid/cloture", headers = Map("X-Auth-Token" -> "abcdef12456"),
          protocol = "http", port = Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(OK)

        whenReady(requester.clotureRequest(uuid)) { res =>
          res shouldEqual((): Unit)
          verify(xivoWS).post("configIP", s"api/1.0/callback_requests/$uuid/cloture", null, null, null, Map("X-Auth-Token" -> "abcdef12456"), Some(9000), "http")
        }
      }
    }

    "uncloture a CallbackRequest" in new Helper {
      running(fakeApp) {
        val wsRequest = mock[WSRequestHolder]
        val response = mock[WSResponse]
        val uuid = UUID.randomUUID()
        stub(xivoWS.post("configIP", s"api/1.0/callback_requests/$uuid/uncloture", headers = Map("X-Auth-Token" -> "abcdef12456"),
          protocol = "http", port = Some(9000))).toReturn(wsRequest)
        stub(wsRequest.execute()).toReturn(Future.successful(response))
        stub(response.status).toReturn(OK)

        whenReady(requester.unclotureRequest(uuid)) { res =>
          res shouldEqual((): Unit)
          verify(xivoWS).post("configIP", s"api/1.0/callback_requests/$uuid/uncloture", null, null, null, Map("X-Auth-Token" -> "abcdef12456"), Some(9000), "http")
        }
      }
    }
  }
}
