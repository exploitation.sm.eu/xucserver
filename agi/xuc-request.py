#!/usr/bin/python
# -*- coding: UTF-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__version__   = '$Revision$'
__date__      = '$Date$'
__copyright__ = 'Copyright (C) 2014-2015 Avencall'
__author__    = 'AV'


import urllib2
import sys
import requests
import argparse
import logging
import traceback
import json
from xivo.agi import AGI

agi = AGI()

XUC_SERVER='192.168.56.1:9000'
TogglePauseURL="http://" + XUC_SERVER + "/xuc/api/1.0/togglePause/"
AgentLoginURL="http://" + XUC_SERVER + "/xuc/api/1.0/agentLogin/"
AgentLogoutURL="http://" + XUC_SERVER + "/xuc/api/1.0/agentLogout/"

#
# User configuration
# -------------

DEBUG_MODE = True # False 
LOGFILE = '/var/log/xivo-xuc-agent-webservices.log'
# timeout of the tcp connection to the webserver (seconds)
CONNECTION_TIMEOUT = 2
#
# Factory configuration
# -------------

URL_HEADERS = { 'User-Agent' : 'XiVO Webservice AGI' }

#logger = logging.getLogger()


class Syslogger(object):

    def write(self, data):
        global logger
        logger.error(data)

def init_logging(debug_mode):
    if debug_mode:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logfilehandler = logging.FileHandler(LOGFILE)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logfilehandler.setFormatter(formatter)
    logger.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger

def send_request(url, customHeaders, params):
    try:
        result = requests.post(url,
                               headers = customHeaders,
                               data = json.dumps(params),
                               timeout=CONNECTION_TIMEOUT)
        agi.verbose("request sent to server")
    except requests.exceptions.ConnectionError:
        agi.verbose("Server XUC refused connection")
        error_cause = "Refused connection"

    except requests.exceptions.Timeout:
        agi.verbose("Timeout when connecting to the XUC server.")
        error_cause = "Timeout"

    except:
        agi.verbose("Unknown problem during connection to the XUC server.")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.debug(repr(traceback.format_exception(exc_type, exc_value,
                                                     exc_traceback)))
        error_cause = "Connection failed to WS"
    finally:
        agi.set_variable("WS_RESULT_CODE",str(result.status_code))

    if toggle_pause_request != None:
        if toggle_pause_request.status_code != requests.codes.ok:
            if toggle_pause_request.status_code == requests.codes.not_found:
                error_cause = "No entry found for " + str(phonenumber)
                logger.warning("No entry found for " + str(phonenumber) + " on server XUC")
            else:
                logger.error("Server XUC returned following error: " + str(toggle_pause_request.status_code))
                error_cause = "Server XUC returned following error: " + str(toggle_pause_request.status_code)

        else:
            agi.verbose("Succesful request for " + str(phonenumber))
    else:
        if error_cause == None:
            error_cause = "Connection problem"
            logger.error("Unknown problem during connection to the XUC server")

def toggle_pause(phonenumber):
    params = {'phoneNumber': phonenumber}
    headers = {'content-type': 'application/json'}

    toggle_pause_request = None
    
    agi.verbose("Toggle Pause request for agent at phone: " + str(phonenumber))

    send_request(TogglePauseURL, headers, params)

def log_agent(phonenumber, agentnumber):
    params = {'agentphonenumber': phonenumber, 'agentnumber': agentnumber}
    headers = {'content-type': 'application/json'}
    agi.verbose("Agent login request for agentnumber: " + str(agentnumber) + " at phone: " + str(phonenumber))
    send_request(AgentLoginURL, headers, params)

def logout_agent(phonenumber):
    params = {'phoneNumber': phonenumber}
    headers = {'content-type': 'application/json'}
    agi.verbose("Agent logout request for agent logged on phone: " + str(phonenumber))
    send_request(AgentLogoutURL, headers, params)

#Timeout et tous les cas ou on n'a pas de réponse
def process_no_response(phonenumber, error_cause):
    logger.warning("Got no response for phonenumber: " + str(phonenumber) + "!")
    sys.exit(1)

def main():
    agi.verbose('---------------------------------------------------')
    #init_logging(DEBUG_MODE)
    try:
        if len(sys.argv) < 2:
            logger.error("wrong number of arguments")
            sys.exit(1)
        agi.verbose('Request: '+sys.argv[1])
        if sys.argv[1] == 'togglePause':
            agi.verbose('togglePause')
            toggle_pause(sys.argv[2])
        elif sys.argv[1] == 'agentLogin':
            if len(sys.argv) < 3:
                agi.verbose('wrong number of arguments')
                logger.error('wrong number of arguments')
                sys.exit(1)
            agi.verbose('agentLogin')
            log_agent(sys.argv[2], sys.argv[3])
        elif sys.argv[1] == 'agentLogout':
            agi.verbose('agentLogout')
            logout_agent(sys.argv[2])
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.error(repr(traceback.format_exception(exc_type, exc_value,
                                          exc_traceback)))
        sys.exit(1)

    sys.exit(1)


if __name__ == '__main__':
    main()

