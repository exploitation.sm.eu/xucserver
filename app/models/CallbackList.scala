package models

import java.util.UUID

import org.joda.time.{LocalDate, LocalTime}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class PreferredCallbackPeriod(uuid: Option[UUID], name: String, periodStart: LocalTime, periodEnd: LocalTime, default: Boolean)

object PreferredCallbackPeriod {
  implicit val reads: Reads[PreferredCallbackPeriod] = (
      (JsPath \ "uuid").readNullable[UUID] and
      (JsPath \ "name").read[String] and
      (JsPath \ "periodStart").read[String].map(LocalTime.parse) and
      (JsPath \ "periodEnd").read[String].map(LocalTime.parse) and
      (JsPath \ "default").read[Boolean]
    )(PreferredCallbackPeriod.apply _)

  implicit val writes: Writes[PreferredCallbackPeriod] = new Writes[PreferredCallbackPeriod] {
    def writes(p: PreferredCallbackPeriod) = Json.obj(
      "uuid" -> p.uuid,
      "name" -> p.name,
      "periodStart" -> p.periodStart.toString("HH:mm:ss"),
      "periodEnd" -> p.periodEnd.toString("HH:mm:ss"),
      "default" -> p.default
    )
  }
}

case class CallbackRequest(uuid: Option[UUID],
                           listUuid: UUID,
                           phoneNumber: Option[String],
                           mobilePhoneNumber: Option[String],
                           firstName: Option[String],
                           lastName: Option[String],
                           company: Option[String],
                           description: Option[String],
                           agentId: Option[Long]=None,
                           queueId: Option[Long]=None,
                           preferredPeriod: Option[PreferredCallbackPeriod]=None,
                           dueDate: LocalDate=LocalDate.now())

object CallbackRequest {
  implicit val writes =  new Writes[CallbackRequest] {
    def writes(cb: CallbackRequest) = Json.obj(
      "uuid" -> cb.uuid,
      "listUuid" -> cb.listUuid,
      "phoneNumber" -> cb.phoneNumber,
      "mobilePhoneNumber" -> cb.mobilePhoneNumber,
      "firstName" -> cb.firstName,
      "lastName" -> cb.lastName,
      "company" -> cb.company,
      "description" -> cb.description,
      "agentId" -> cb.agentId,
      "queueId" -> cb.queueId,
      "preferredPeriod" -> cb.preferredPeriod,
      "dueDate" -> cb.dueDate.toString("yyyy-MM-dd")
    )
  }

  implicit val reads: Reads[CallbackRequest] = (
    (JsPath \ "uuid").readNullable[UUID] and
    (JsPath \ "listUuid").read[String].map(UUID.fromString) and
    (JsPath \ "phoneNumber").readNullable[String] and
    (JsPath \ "mobilePhoneNumber").readNullable[String] and
    (JsPath \ "firstName").readNullable[String] and
    (JsPath \ "lastName").readNullable[String] and
    (JsPath \ "company").readNullable[String] and
    (JsPath \ "description").readNullable[String] and
    (JsPath \ "agentId").readNullable[Long] and
    (JsPath \ "queueId").readNullable[Long] and
    (JsPath \ "preferredPeriod").readNullable[PreferredCallbackPeriod] and
    (JsPath \ "dueDate").readNullable[String].map(_.map(LocalDate.parse)).map(_.getOrElse(LocalDate.now))
  )(CallbackRequest.apply _)
}

case class CallbackList(uuid: Option[UUID],
                        name: String,
                        queueId: Long,
                        callbacks: List[CallbackRequest])

object CallbackList {

  implicit val writes =  new Writes[CallbackList] {
    def writes(cbList: CallbackList) = Json.obj(
    "uuid" -> cbList.uuid,
    "name" -> cbList.name,
    "queueId" -> cbList.queueId,
    "callbacks" -> cbList.callbacks.map(cb => Json.toJson(cb)))
  }

  implicit val reads: Reads[CallbackList] = (
    (JsPath \ "uuid").readNullable[UUID] and
      (JsPath \ "name").read[String] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "callbacks").read[List[CallbackRequest]]
    )(CallbackList.apply _)
}

case class CallbackLists(lists: List[CallbackList])

object CallbackLists {
  implicit val writes = new Writes[CallbackLists] {
    override def writes(l: CallbackLists): JsValue = Json.toJson(l.lists)
  }
}