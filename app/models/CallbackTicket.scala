package models

import java.security.InvalidParameterException
import java.util.UUID

import org.joda.time.{LocalDate, DateTime}
import org.joda.time.format.DateTimeFormat
import play.api.libs.json._
import play.api.libs.functional.syntax._
import services.config.ConfigRepository

object CallbackStatus extends Enumeration {
  type CallbackStatus = Value
  val Fax = Value("fax")
  val NoAnswer = Value("noanswer")
  val Answered = Value("answered")
  val Callback = Value("callback")
  val Voicemail = Value("voicemail")
}

case class CallbackTicket(uuid: Option[UUID],
                          listUuid: UUID,
                          requestUuid: UUID,
                          queueRef: String,
                          agentNum: String,
                          dueDate: LocalDate,
                          started: DateTime,
                          lastUpdate: Option[DateTime],
                          callid: Option[String]=None,
                          status: Option[CallbackStatus.CallbackStatus]=None,
                          comment: Option[String]=None,
                          phoneNumber: Option[String]=None,
                          mobilePhoneNumber: Option[String]=None,
                          firstName: Option[String]=None,
                          lastName: Option[String]=None,
                          company: Option[String]=None,
                          description: Option[String]=None)

object CallbackTicket {
  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  implicit val reads: Reads[CallbackTicket] = (
    (JsPath \ "uuid").readNullable[String].map(_.map(UUID.fromString)) and
    (JsPath \ "listUuid").read[String].map(UUID.fromString) and
    (JsPath \ "requestUuid").read[String].map(UUID.fromString) and
    (JsPath \ "queueRef").read[String] and
    (JsPath \ "agentNum").read[String] and
    (JsPath \ "dueDate").read[String].map(LocalDate.parse) and
    (JsPath \ "started").read[String].map(formatter.parseDateTime) and
    (JsPath \ "lastUpdate").readNullable[String].map(_.map(formatter.parseDateTime)) and
    (JsPath \ "callid").readNullable[String] and
    (JsPath \ "status").readNullable[String].map(_.map(CallbackStatus.withName)) and
    (JsPath \ "comment").readNullable[String] and
    (JsPath \ "phoneNumber").readNullable[String] and
    (JsPath \ "mobilePhoneNumber").readNullable[String] and
    (JsPath \ "firstName").readNullable[String] and
    (JsPath \ "lastName").readNullable[String] and
    (JsPath \ "company").readNullable[String] and
    (JsPath \ "description").readNullable[String]
  )(CallbackTicket.apply _)

  implicit val writes =  new Writes[CallbackTicket] {
    def writes(ticket: CallbackTicket) = Json.obj(
      "uuid" -> ticket.uuid,
      "listUuid" -> ticket.listUuid,
      "requestUuid" -> ticket.requestUuid,
      "queueRef" -> ticket.queueRef,
      "agentNum" -> ticket.agentNum,
      "dueDate" -> ticket.dueDate.toString("yyyy-MM-dd"),
      "started" -> ticket.started.toString(formatter),
      "lastUpdate" -> ticket.lastUpdate.map(_.toString(formatter)),
      "callid" -> ticket.callid,
      "status" -> ticket.status.map(_.toString),
      "comment" -> ticket.comment,
      "phoneNumber" -> ticket.phoneNumber,
      "mobilePhoneNumber" -> ticket.mobilePhoneNumber,
      "firstName" -> ticket.firstName,
      "lastName" -> ticket.lastName,
      "company" -> ticket.company,
      "description" -> ticket.description
    )
  }

  def fromRequest(request: CallbackRequest, agentNumber: String) = {
    request.queueId match {
      case None => throw new InvalidParameterException("queueId is required when creating a CallbackTicket from a CallbackRequest")
      case Some(queueId) => ConfigRepository.repo.getQueue(queueId) match {
        case Some(queue) => CallbackTicket(None, request.listUuid, request.uuid.get, queue.getName, agentNumber, request.dueDate, DateTime.now(), None,
          phoneNumber = request.phoneNumber, mobilePhoneNumber = request.mobilePhoneNumber, description = request.description,
          firstName = request.firstName, lastName = request.lastName, company = request.company)
        case None => throw new InvalidParameterException(s"Could not find queue with id $queueId")
      }
    }
  }
}

case class CallbackTicketPatch(status: Option[CallbackStatus.CallbackStatus], comment: Option[String], callid: Option[String])

object CallbackTicketPatch {
  implicit val writes =  new Writes[CallbackTicketPatch] {
    def writes(patch: CallbackTicketPatch) = Json.obj(
      "status" -> patch.status.map(_.toString),
      "comment" -> patch.comment,
      "callid" -> patch.callid
    )
  }
}