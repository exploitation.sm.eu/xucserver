package models

import play.api.libs.json.{Json, Writes}
import xivo.xucami.models.QueueCall

case class QueueCallList(queueId: Long, calls: List[QueueCall])

object QueueCallList {
  implicit val writes =  new Writes[QueueCallList] {
    def writes(queueCalls: QueueCallList) = Json.obj(
      "queueId" -> queueCalls.queueId,
      "calls" -> queueCalls.calls.map(call => Json.obj(
        "position" -> call.position,
        "name" -> call.name,
        "number" -> call.number,
        "queueTime" -> call.queueTime.toString()))
    )
  }
}
