package controllers.security

import java.security.PrivilegedExceptionAction
import javax.security.auth.Subject
import javax.security.auth.kerberos.KerberosPrincipal
import javax.security.auth.login.{LoginContext, LoginException}

import org.apache.xerces.impl.dv.util.Base64
import org.ietf.jgss._
import org.slf4j.LoggerFactory
import play.api.Logger
import play.api.mvc._

import scala.collection.JavaConversions._

trait KerberosAuthentication extends Results {

  val securedLoginConfiguration = new SecuredLoginConfiguration
  val serverSubject = performServerLogin
  val spnegoOid = new Oid("1.3.6.1.5.5.2")
  val gssManager = GSSManager.getInstance
  val logger = LoggerFactory.getLogger(getClass)

  private def performServerLogin: Subject = {
    val principals = Set(new KerberosPrincipal(securedLoginConfiguration.getPrincipal()))
    val subject: Subject = new Subject(false, principals, Set[AnyRef](), Set[AnyRef]())
    val loginContext = new LoginContext("", subject, null, securedLoginConfiguration)
    try {
      loginContext.login()
    } catch {
      case e: LoginException =>  Logger.error("Error during server Kerberos login", e)
    }
    loginContext.getSubject
  }

  def performKerberosAuthentication(request: RequestHeader): Option[String] = request.headers.get("Authorization") match {
    case None => None
    case Some(headerValue) =>
      try {
        logger.debug("Header exists : perform login")
        performClientLogin(headerValue)
      } catch {
        case e: Exception => {
          logger.error("Received exception when performing the Kerberos authentication",e)
          None
        }
      }
  }

  private def performClientLogin(authorizationString: String): Option[String] = {
    val context: GSSContext = createClientContext(authorizationString)
    if (context.isEstablished) {
      val login = context.getSrcName.toString
      if(login.contains("@"))
        return Some(login.split("@")(0))
      return Some(login)
    }
    None
  }

  private def createClientContext(authorizationString: String): GSSContext = Subject.doAs(serverSubject, new PrivilegedExceptionAction[GSSContext] {
    def run: GSSContext = {
      val serverCreds: GSSCredential = gssManager.createCredential(null, GSSCredential.DEFAULT_LIFETIME, spnegoOid, GSSCredential.ACCEPT_ONLY)
      val authorization = authorizationString.substring(authorizationString.indexOf(" ") + 1)
      val authorizationBytes: Array[Byte] = Base64.decode(authorization)
      val context: GSSContext = gssManager.createContext(serverCreds)
      context.acceptSecContext(authorizationBytes, 0, authorizationBytes.length)
      context
    }
  })

}
