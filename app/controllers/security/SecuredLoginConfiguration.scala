package controllers.security

import javax.security.auth.login.{AppConfigurationEntry, Configuration}

import play.Play

import scala.collection.JavaConversions._

class SecuredLoginConfiguration extends Configuration {

  def getPrincipal(): String = {
    val principal =  Play.application.configuration.getString("secured.krb5.principal")
    if(principal == null) throw new NullPointerException("Principal cannot be null")     
     principal
  }  

  val options = Map(
      "keyTab" -> Play.application.configuration.getString("secured.krb5.keyTab"),
      "principal" -> getPrincipal(),
      "password" -> Play.application.configuration.getString("secured.krb5.password"),
      "useKeyTab" -> "true",
      "storeKey" -> "true",
      "doNotPrompt" -> "true",
      "useTicketCache" -> "false",
      "renewTGT" -> "false",
      "isInitiator" -> "false",
      "debug" -> Play.application.configuration.getString("secured.krb5.debug", "false"))

  val appConfigurationEntry = Array(new AppConfigurationEntry("com.sun.security.auth.module.Krb5LoginModule",
    AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, options))

   def getAppConfigurationEntry(name: String): Array[AppConfigurationEntry] = appConfigurationEntry
}