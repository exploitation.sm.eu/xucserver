package controllers.helpers

import com.googlecode.htmlcompressor.compressor.HtmlCompressor
import play.Play
import play.twirl.api.Html


object PrettyController {

  def prettify(html: Html) : Html = {
    val compressor = new HtmlCompressor()
    val cleanBody = html.body.trim()

    if (Play.application().isDev()) {
      compressor.setPreserveLineBreaks(true)
      compressor.setRemoveIntertagSpaces(false)
      compressor.setRemoveComments(false)
    }

    val compressedBody = compressor.compress(cleanBody)

    Html(compressedBody)

  }

}