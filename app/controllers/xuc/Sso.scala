package controllers.xuc

import akka.pattern.{AskTimeoutException, ask}
import controllers.security.KerberosAuthentication
import play.api.libs.concurrent.Akka
import play.api.mvc.{Action, Controller}
import play.mvc.Http
import xivo.services.TokenRetriever
import xivo.xuc.api.{RequestError, RequestResult, RequestSuccess, RequestUnauthorized}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import play.api.Play.current

object Sso extends Controller with KerberosAuthentication {

  val tokenRetriever = Akka.system.actorOf(TokenRetriever.props)
  implicit val timeout: akka.util.Timeout = 2 seconds

  def createToken(orig: String) = Action.async {
    implicit request => performKerberosAuthentication(request) match {
      case Some(login) => generateToken(login).map({
        case RequestSuccess(token) => Redirect(s"$orig?token=$token")
        case RequestError(reason) => InternalServerError(reason)
        case RequestUnauthorized(reason) => Unauthorized(reason)
      }).recover({
        case e: AskTimeoutException => InternalServerError("Timeout while getting the token")
      })
      case None => Future.successful(Unauthorized("You are unauthorized").withHeaders(Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate"))
    }
  }

  private def generateToken(login: String): Future[RequestResult] = (tokenRetriever ? TokenRetriever.TokenByLogin(login)).mapTo[RequestResult]
}
