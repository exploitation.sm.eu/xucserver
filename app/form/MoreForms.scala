package form

import play.api.data.Mapping
import play.api.data.Forms._
import play.api.data.format._
import play.api.data.format.Formats._
import play.api.Logger
import form.MoreFormats._

object MoreForms {

  def bigDecimalWithSpaces( precision : Int, scale: Int ): Mapping[BigDecimal] = of[BigDecimal] as myBigDecimalFormat(Some(precision,scale))

}
