package services.directory

import models._
import org.xivo.cti.model.PhoneHintStatus
import services.config.ConfigRepository

import scala.collection.mutable

class DirectoryTransformerLogic(repo: ConfigRepository) {
  private def enrich(entry: ResultItem): RichEntry = {
    val lineNumber = entry.relations.endpointId.fold[Option[String]](None)(lineId => repo.linePhoneNbs.get(lineId))
    val phoneStatus = lineNumber.fold(PhoneHintStatus.UNEXISTING)(lineNumber =>
      repo.richPhones.get(lineNumber).getOrElse(PhoneHintStatus.UNEXISTING))
    val item = entry.item
    RichEntry(
      phoneStatus,
      mutable.Buffer(item.name, item.number.getOrElse(""), item.mobile.getOrElse(""), item.externalNumber.getOrElse(""),
                     item.favorite, item.email.getOrElse("")),
      entry.relations.sourceEntryId,
      Some(entry.source),
      Some(item.favorite)
    )
  }

  def addStatusesDirResult(directoryResult: DirSearchResult): RichDirectoryResult = {
    val result = new RichDirectoryResult(RichDirectoryEntries.removeFavorite(directoryResult.columnHeaders))
    val entries = directoryResult.results
    entries.foreach(entry => { result.add(enrich(entry)) })
    result
  }

  def addStatusesFavorites(directoryResult: DirSearchResult): RichFavorites = {
    val result = new RichFavorites(RichDirectoryEntries.removeFavorite(directoryResult.columnHeaders))
    val entries = directoryResult.results
    entries.foreach(entry => { result.add(enrich(entry)) })
    result
  }

}
