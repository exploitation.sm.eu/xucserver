package services.config

import java.util.UUID

import models.{CallbackList, CallbackRequest}
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.libs.ws.WSResponse
import xivo.models.Agent
import xivo.network.{WebServiceException, XiVOWSdef}
import xivo.xuc.XucConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ConfigServerRequester(ws: XiVOWSdef) {
  val logger = LoggerFactory.getLogger(getClass)

  def getCallbackLists: Future[List[CallbackList]] = {
    ws.get(XucConfig.configHost, "api/1.0/callback_lists", port=Some(XucConfig.configPort),
    headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http").execute().map(response => {
      response.json.validate[List[CallbackList]] match {
        case e: JsError => logger.error(s"Could not read from config server : $e")
          throw new WebServiceException("Non understandable JSON returned")
        case s: JsSuccess[List[CallbackList]] => s.get
      }
    })
  }

  def importCsvCallback(listUuid: String, csv: String): Future[Unit] = {
    ws.genericPost(XucConfig.configHost, s"api/1.0/callback_lists/$listUuid/callback_requests/csv", port=Some(XucConfig.configPort),
    headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http", payload=Some(csv))
      .execute().map(processPotentialError)
  }

  def takeCallback(uuid: String, agent: Agent): Future[Unit] = ws.post(XucConfig.configHost, s"api/1.0/callback_requests/$uuid/take", port=Some(XucConfig.configPort),
  headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http", payload=Some(Json.obj("agentId" -> agent.id)))
    .execute().map(processPotentialError)

  def releaseCallback(uuid: String): Future[Unit] = ws.post(XucConfig.configHost, s"api/1.0/callback_requests/$uuid/release", port=Some(XucConfig.configPort),
  headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http").execute().map(processPotentialError)

  def getCallbackRequest(uuid: String): Future[CallbackRequest] = ws.get(XucConfig.configHost, s"api/1.0/callback_requests/$uuid", port=Some(XucConfig.configPort),
  headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http").execute().map(response => {
    processPotentialError(response)
    response.json.validate[CallbackRequest] match {
      case e: JsError => logger.error(s"Could not read from config server : $e")
        throw new WebServiceException("Non understandable JSON returned")
      case s: JsSuccess[CallbackRequest] => s.get
    }
  })

  def clotureRequest(uuid: UUID): Future[Unit] = ws.post(XucConfig.configHost, s"api/1.0/callback_requests/$uuid/cloture", port=Some(XucConfig.configPort),
    headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http")
    .execute().map(processPotentialError)

  def unclotureRequest(uuid: UUID): Future[Unit] = ws.post(XucConfig.configHost, s"api/1.0/callback_requests/$uuid/uncloture", port=Some(XucConfig.configPort),
    headers=Map("X-Auth-Token" -> XucConfig.configToken), protocol="http")
    .execute().map(processPotentialError)

  private def processPotentialError(response: WSResponse): Unit = {
    if(response.status >=400)
    throw new WebServiceException(s"""Web service call failed with code ${response.status} and message "${response.body}"""")
  }

}
