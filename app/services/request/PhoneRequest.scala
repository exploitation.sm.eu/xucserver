package services.request

import play.api.libs.json.{JsPath, JsSuccess, JsValue, Json}
import play.api.libs.functional.syntax._

abstract class PhoneRequest extends XucRequest

object PhoneRequest {
  case class Dial(destination: String, variables: Map[String, String]=Map()) extends PhoneRequest
  object Dial {
    implicit val reads = ((JsPath \ "destination").read[String] and
      (JsPath \ "variables").readNullable[Map[String, String]].map(_.getOrElse(Map())))(Dial.apply _)
    def validate(json: JsValue) = json.validate[Dial]
  }

  case object Answer extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(Answer)
  }

  case object Hangup extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(Hangup)
  }

  case class AttendedTransfer(destination: String) extends PhoneRequest
  object AttendedTransfer {
    implicit val reads = Json.reads[AttendedTransfer]
    def validate(json: JsValue) = json.validate[AttendedTransfer]
  }

  case object CompleteTransfer extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(CompleteTransfer)
  }

  case object CancelTransfer extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(CancelTransfer)
  }

  case object Conference extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(Conference)
  }

  case object Hold extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(Hold)
  }
}
