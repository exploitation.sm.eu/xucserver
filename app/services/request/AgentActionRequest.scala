package services.request

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsValue, JsPath, Reads}
import xivo.models.Agent

class AgentActionRequest  extends XucRequest

case class AgentListen(id: Agent.Id, fromUser: Option[Long]=None)  extends AgentActionRequest

object AgentListen{
  def validate(json: JsValue) = json.validate[AgentListen]

  implicit val AgentListenRead : Reads[AgentListen] = (JsPath \ "agentid").read[Long].map(AgentListen.apply(_) )

}

case class AgentLoginRequest(id: Option[Agent.Id], phoneNumber : Option[String], agentNumber : Option[Agent.Number] = None) extends AgentActionRequest



object AgentLoginRequest {

  def validate(json: JsValue) = json.validate[AgentLoginRequest]

  val allFields: Reads[AgentLoginRequest] = (
    (JsPath \ "agentid").readNullable[Agent.Id] and
      (JsPath \ "agentphonenumber").read[Option[String]] and
      (JsPath \ "agentnumber").read[Option[Agent.Number]]
    )(AgentLoginRequest.apply _)

  val onlyAgentId: Reads[AgentLoginRequest] = {
    (JsPath \ "agentid").read[Long].map(id => AgentLoginRequest.apply(Some(id),None))
  }
  val onlyPhoneNumber: Reads[AgentLoginRequest] = {
    (JsPath \ "agentphonenumber").read[String].map(phoneNb => AgentLoginRequest.apply(None, Some(phoneNb)))
  }
  val onlyPhoneNumberAsLong: Reads[AgentLoginRequest] = {
    (JsPath \ "agentphonenumber").read[Long].map(phoneNb => AgentLoginRequest.apply(None, Some(phoneNb.toString)))
  }
  val agentIdAndAgentPhoneNumber: Reads[AgentLoginRequest] = {
    ((JsPath \ "agentid").read[Long] and
      (JsPath \ "agentphonenumber").read[String]
      ) ((id,phoneNb) => AgentLoginRequest.apply(Some(id), Some(phoneNb)) )
  }

  implicit val AgentLoginRequestRead: Reads[AgentLoginRequest] =
    allFields orElse agentIdAndAgentPhoneNumber orElse onlyAgentId orElse onlyPhoneNumber orElse onlyPhoneNumberAsLong
}

case class AgentLogout(phoneNumber: String) extends AgentActionRequest

case class AgentTogglePause(phoneNumber: String) extends AgentActionRequest
