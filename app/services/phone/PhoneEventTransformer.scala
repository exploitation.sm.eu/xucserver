package services.phone

import akka.actor.{Props, ActorLogging, Actor}
import services.{XucEventBus, XucAmiBus}
import services.XucAmiBus.{ChannelEvent, AmiType}
import xivo.events.{UserData, PhoneEventType, PhoneEvent}
import xivo.xucami.models.{Channel, ChannelState}

object PhoneEventTransformer {
  def props = Props(new PhoneEventTransformer(XucEventBus.bus, XucAmiBus.amiEventBus))
}

class PhoneEventTransformer(val eventBus: XucEventBus, val amiBus: XucAmiBus, val attachedFilter:UserData.filter = UserData.filterData) extends Actor with ActorLogging {

  amiBus.subscribe(self, AmiType.ChannelEvent)

  private def transform(channel:Channel):Option[PhoneEvent] =
    channel.state match {
      case ChannelState.RINGING if channel.variables.get("XUC_CALLTYPE") == Some("Originate") =>
        Some(PhoneEvent(PhoneEventType.EventDialing, channel.callerId.number, channel.connectedLineNb.getOrElse(""),
          channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case ChannelState.RINGING if !channel.isAgentCallback() =>
        Some(PhoneEvent(PhoneEventType.EventRinging, channel.callerId.number, channel.connectedLineNb.getOrElse(""), channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case ChannelState.UP if channel.variables.get("XUC_CALLTYPE") == Some("Originate") && channel.variables.get("XIVO_CALLOPTIONS").isDefined =>
        Some(PhoneEvent(PhoneEventType.EventEstablished, channel.callerId.number, channel.connectedLineNb.getOrElse(""),
          channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case ChannelState.UP if channel.variables.get("XUC_CALLTYPE") == Some("Originate") =>
        Some(PhoneEvent(PhoneEventType.EventDialing, channel.callerId.number, channel.connectedLineNb.getOrElse(""),
          channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case ChannelState.UP  if !channel.isAgentCallback() =>
        Some(PhoneEvent(PhoneEventType.EventEstablished, channel.callerId.number, channel.connectedLineNb.getOrElse(""),
          channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case ChannelState.HUNGUP  =>
        Some(PhoneEvent(PhoneEventType.EventReleased, channel.callerId.number, channel.connectedLineNb.getOrElse(""),
          channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case ChannelState.ORIGINATING =>
        Some(PhoneEvent(PhoneEventType.EventDialing, channel.callerId.number, channel.exten.getOrElse(""),
          channel.linkedChannelId, channel.id,
          queueName = channel.variables.get(UserData.QueueNameKey),
          userData = attachedFilter(channel.variables)))

      case _ =>  None
    }

  def receive = {
    case ChannelEvent(channel) =>
      log.debug(s"->$channel")
      transform(channel) map (eventBus.publish(_))
    case _ =>
  }

}
