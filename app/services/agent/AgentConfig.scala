package services.agent

import akka.actor.{Actor, ActorLogging, Props}
import services.ProductionActorFactory
import services.agent.AgentInGroupAction.{RemoveAgents, AgentsDestination}
import services.request._

object AgentConfig {
  def props = Props(new AgentConfig)
}

class AgentConfig extends Actor with ActorLogging with ProductionActorFactory {

  override def receive: Receive = {

    case BaseRequest(_,MoveAgentInGroup(groupId,fromQueueId,fromPenalty,toQueueId,toPenalty)) =>
      getMoveAgentInGroup(groupId, fromQueueId, fromPenalty) ! AgentsDestination(toQueueId, toPenalty)

    case BaseRequest(_,AddAgentInGroup(groupId,fromQueueId,fromPenalty,toQueueId,toPenalty)) =>
      getAddAgentInGroup(groupId, fromQueueId, fromPenalty) ! AgentsDestination(toQueueId, toPenalty)

    case BaseRequest(_,AddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty)) =>
      getAddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty) ! AgentsDestination(toQueueId, toPenalty)

    case BaseRequest(_,RemoveAgentGroupFromQueueGroup(groupId,queueId,penalty)) =>
      getRemoveAgentGroupFromQueueGroup(groupId, queueId, penalty) ! RemoveAgents

    case BaseRequest(_, SetAgentGroup(gId, aId)) => getAgentGroupSetter() ! SetAgentGroup(gId, aId)

    case unknown => log.debug(s"unkown $unknown message received")
  }
}
