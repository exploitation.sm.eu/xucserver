package xivo.network

import models.{CallbackTicket, CallbackTicketPatch}
import org.slf4j.LoggerFactory
import play.api.Play.current
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.libs.ws.WS

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class RecordingWS(host: String, port: Int, token: String) {
  val logger = LoggerFactory.getLogger(getClass)

  def getCallHistory(size: Int, agentNumber: String): Future[HistoryServerResponse] = {
    val json = Json.obj("agent" -> agentNumber)
    WS.url(s"http://$host:$port/records/search?page=1&pageSize=$size").withHeaders("X-Auth-Token" -> token).post(json).map(response => {
      if (response.status == 200) HistoryServerResponse(Json.parse(response.body) \ "records")
      else if (response.status == 403) throw new ForbiddenException
      else throw new WebServiceException(s"Request to history web service failed with status ${response.status} and message ${response.body}")
    })
  }

  def getUserCallHistory(size: Int, interface: String): Future[HistoryServerResponse] = {
    val json = Json.obj("interface" -> interface)
    WS.url(s"http://$host:$port/history?size=$size").withHeaders("X-Auth-Token" -> token).post(json).map(response => {
      if (response.status == 200) HistoryServerResponse(Json.parse(response.body))
      else if (response.status == 403) throw new ForbiddenException
      else throw new WebServiceException(s"Request to history web service failed with status ${response.status} and message ${response.body}")
    })
  }

  def createCallbackTicket(ticket: CallbackTicket): Future[CallbackTicket] = WS.url(s"http://$host:$port/callback_tickets")
    .withHeaders("X-Auth-Token" -> token).post(Json.toJson(ticket)).map(response => {
    response.json.validate[CallbackTicket] match {
      case e: JsError => logger.error(s"Could not read from config server : $e")
        throw new WebServiceException("Non understandable JSON returned")
      case s: JsSuccess[CallbackTicket] => s.get
    }
  })

  def updateCallbackTicket(uuid: String, patch: CallbackTicketPatch): Future[Unit] = WS.url(s"http://$host:$port/callback_tickets/$uuid")
    .withHeaders("X-Auth-Token" -> token).put(Json.toJson(patch)).map(response => {
    if(response.status == 200) Unit
    else throw new WebServiceException(response.body)
  })

  def exportTicketsCSV(listUuid: String): Future[String] = WS.url(s"http://$host:$port/callback_tickets/search?listUuid=$listUuid")
    .withHeaders("X-Auth-Token" -> token).post("").map(response => {
    if(response.status == 200) response.body
    else throw new WebServiceException(response.body)
  })

  def getCallbackTicket(uuid: String): Future[CallbackTicket] = WS.url(s"http://$host:$port/callback_tickets/$uuid")
    .withHeaders("X-Auth-Token" -> token).get().map(response => {
    response.json.validate[CallbackTicket] match {
      case e: JsError => logger.error(s"Could not read from config server : $e")
        throw new WebServiceException("Non understandable JSON returned")
      case s: JsSuccess[CallbackTicket] => s.get
    }
  })
}

case class HistoryServerResponse(json: JsValue)

class ForbiddenException extends Exception("Cannot login to history server, probably wrong login/password")
class WebServiceException(message: String) extends Exception(message: String) {
  override def equals(o: Any) = o match {
    case e: WebServiceException => e.getMessage == message
    case _ => false
  }
}
