package xivo.phonedevices

import xivo.models.Line
import xivo.models.XivoDevice

class DeviceAdapterFactory {

  def getAdapter(line: Line): DeviceAdapter = lineAdapter(line).getOrElse(defaultAdapter(line))

  private def lineAdapter(line: Line): Option[DeviceAdapter] =
    (for {
      device <- line.dev
      ip <- device.ip
    } yield vendorAdapter(device.vendor,ip)).flatten

  private def vendorAdapter(vendor : String, ip : String): Option[DeviceAdapter] = vendor match {
    case "Snom" =>   Some(new SnomDevice(ip))
    case _ => None
  }

  private def defaultAdapter(line:Line) = new CtiDevice(line)
}