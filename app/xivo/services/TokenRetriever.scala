package xivo.services

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.ask
import models.Token
import services.config.ConfigRepository
import services.{ActorFactory, ProductionActorFactory}
import xivo.services.TokenRetriever.TokenByLogin
import xivo.services.XivoAuthentication.{AuthUnknownUser, AuthTimeSynchronizationError, AuthTimeout}
import xivo.xuc.api.{RequestUnauthorized, RequestError, RequestSuccess}
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global


object TokenRetriever {
  def props = Props(new TokenRetriever with ProductionActorFactory)

  case class TokenByLogin(login: String)
}

class TokenRetriever extends Actor with ActorLogging {
  this: ActorFactory =>

  implicit val timeout: akka.util.Timeout = 2 seconds

  override def receive: Receive = {
    case TokenByLogin(login) => val theSender = sender
      ConfigRepository.repo.getUser(login) match {
        case Some(u) =>
          val res = context.actorSelection(xivoAuthenticationURI) ? XivoAuthentication.GetToken(u.id)
          res.map({
            case Token(token, _, _, _, _) => theSender ! RequestSuccess(token)
            case AuthTimeout => theSender ! RequestError("Token retrieval timeout")
            case AuthTimeSynchronizationError => theSender ! RequestError("Token time synchronization error")
            case AuthUnknownUser => theSender ! RequestUnauthorized(s"User $login unknown by the token server")
          })
        case None => theSender ! RequestUnauthorized(s"User $login unknown by the Xuc")
      }
  }
}
