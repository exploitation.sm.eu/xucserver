package xivo.models

import anorm.SqlParser._
import anorm._
import play.api.Logger
import play.api.db.DB
import play.api.http.Status._
import play.api.libs.functional.syntax._
import play.api.libs.json.Writes._
import play.api.libs.json.{Json, _}
import xivo.network.XiVOWS
import scala.concurrent.duration.DurationInt
import play.api.Play.current


import scala.concurrent.Await

case class AgentQueueMember(agentId: Agent.Id, queueId: Long, penalty: Int)

trait AgentQueueMemberFactory  {
  def removeAgentFromQueue(agentId: Agent.Id, queueId: Long ): Option[AgentQueueMember]
  def setAgentQueue(agentId: Agent.Id, queueId: Long, penalty: Int) : Option[AgentQueueMember]
  def all() : List[AgentQueueMember]
  def getQueueMember(agentNumber: String, queueName: String):Option[AgentQueueMember]
}
object AgentQueueMember extends AgentQueueMemberFactory {
  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext
  val logger = Logger(getClass.getName)
  val waitResult = 10 seconds

  implicit val agqmWrites = (
    (__ \ "agentId").write[Long] and
    (__ \ "queueId").write[Long] and
    (__ \ "penalty").write[Int])(unlift(AgentQueueMember.unapply))

  implicit val agqmReads: Reads[AgentQueueMember] = (
      (JsPath \ "agent_id").read[Agent.Id] and
      (JsPath \ "queue_id").read[Long] and
      (JsPath  \ "penalty").read[Int]
    )(AgentQueueMember.apply _)

  def toJson(agentQueueMember: AgentQueueMember) =  Json.toJson(agentQueueMember)
  def toJson(agentQueueMembers : List[AgentQueueMember]) = Json.toJson(agentQueueMembers)

  def removeAgentFromQueue(agentId: Agent.Id, queueId: Long ): Option[AgentQueueMember] = {
    val responseFuture = XiVOWS.withWS(s"queues/$queueId/members/agents/$agentId").delete()
    val resultFuture = responseFuture map { response =>
      response.status match {
        case NO_CONTENT => Some(AgentQueueMember(agentId, queueId, -1))
        case _ =>
          logger.error(s"Webservice unable to remove agent  $agentId from queue $queueId")
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def associateAgentToQueue(agentId: Agent.Id, queueId: Long, penalty: Int): Option[AgentQueueMember] = {
    val data = Json.obj(
      "agent_id" -> agentId,
      "penalty" -> penalty
    )
    val responseFuture = XiVOWS.withWS(s"queues/$queueId/members/agents")
      .post(data)
    val resultFuture = responseFuture map { response =>
      response.status match {
        case CREATED => getAgentAssociation(agentId, queueId)
        case _ =>
          logger.error(s"Webservice unable to set agent association for agent: $agentId queue $queueId failed.")
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def setAgentQueue(agentId: Agent.Id, queueId: Long, penalty: Int) : Option[AgentQueueMember] = {

    val data = Json.obj(
      "penalty" -> penalty
    )

    val responseFuture = XiVOWS.withWS(s"queues/$queueId/members/agents/$agentId").put(data)
    val resultFuture = responseFuture map { response =>
      response.status match {
        case NOT_FOUND => associateAgentToQueue(agentId, queueId, penalty)
        case NO_CONTENT => getAgentAssociation(agentId, queueId)
        case _ =>
          logger.error(s"Webservice set agent association for agent: $agentId queue $queueId failed.")
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def getAgentAssociation(agentId : Agent.Id, queueId: Long) : Option[AgentQueueMember] = {
    val responseFuture = XiVOWS.withWS(s"queues/$queueId/members/agents/$agentId").get()
    val resultFuture = responseFuture map { response =>
      response.status match {
        case OK =>
          val queueMember = (response.json).validate[AgentQueueMember].get
          Some(queueMember)
        case default => {
          logger.error(s"Webservice get agent association for agent: $agentId queue $queueId failed.")
          None
        }
      }
    }
    Await.result(resultFuture, waitResult)
  }

  val simple = {
    get[Long]("agentid") ~
      get[Long]("queueid") ~
      get[Int]("penalty") map {
      case agentId ~ queueId ~ penalty =>
        AgentQueueMember(agentId, queueId, penalty)
    }
  }

  def all():List[AgentQueueMember] =
    DB.withConnection { implicit c =>
      SQL(
        """select userid as agentid, id as queueid, penalty from queuemember, queuefeatures
          where queuemember.usertype='agent' and queuefeatures.name = queuemember.queue_name""")
        .as(simple *)
    }

  private def query(agentNumber: String, queueName: String) = s"""
      select userid as agentid, id as queueid, penalty from queuemember, queuefeatures
      where split_part(interface,'/',2) = '$agentNumber' and queue_name = '$queueName'
      and queuefeatures.name = queuemember.queue_name
      """

  def getQueueMember(agentNumber: String, queueName: String):Option[AgentQueueMember] =
    DB.withConnection {
      implicit c =>
        SQL(query(agentNumber, queueName)).as(simple.singleOpt)
    }
}