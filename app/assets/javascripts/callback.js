var Callback = {

    init: function(cti) {
        this.MessageFactory.init(cti.WebsocketMessageFactory);
        this.sendCallback = cti.sendCallback;
    },
    getCallbackLists: function() {
        var message = this.MessageFactory.createGetCallbackLists();
        this.sendCallback(message);
    },
    takeCallback: function(uuid) {
        var message = this.MessageFactory.createTakeCallback(uuid);
        this.sendCallback(message);
    },
    releaseCallback: function(uuid) {
        var message = this.MessageFactory.createReleaseCallback(uuid);
        this.sendCallback(message);
    },
    startCallback: function(uuid, number) {
        var message = this.MessageFactory.createStartCallback(uuid, number);
        this.sendCallback(message);
    },
    updateCallbackTicket: function(uuid, status, comment) {
        var message = this.MessageFactory.createUpdateCallbackTicket(uuid, status, comment);
        this.sendCallback(message);
    },

    MessageType: {
        CALLBACKLISTS: 'CallbackLists',
        CALLBACKTAKEN: 'CallbackTaken',
        CALLBACKRELEASED: 'CallbackReleased',
        CALLBACKSTARTED: 'CallbackStarted',
        CALLBACKCLOTURED: 'CallbackClotured'
    },

    MessageFactory: {
        init: function(ctiMessageFactory) {
            this.ctiMessageFactory = ctiMessageFactory;
        },
        createMessage: function(command) {
            return this.ctiMessageFactory.createMessage(command);
        },
        createGetCallbackLists: function() {
            return this.createMessage("getCallbackLists");
        },
        createTakeCallback: function(uuid) {
            var message = this.createMessage("takeCallback");
            message.uuid = uuid;
            return message;
        },
        createReleaseCallback: function(uuid) {
            var message = this.createMessage("releaseCallback");
            message.uuid = uuid;
            return message;
        },
        createStartCallback: function(uuid, number) {
            var message = this.createMessage("startCallback");
            message.uuid = uuid;
            message.number = number;
            return message;
        },
        createUpdateCallbackTicket: function(uuid, status, comment) {
            var message = this.createMessage("updateCallbackTicket");
            message.uuid = uuid;
            message.status = status;
            message.comment = comment;
            return message;
        }
    }
};